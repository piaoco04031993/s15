// alert('hi')

// Assignment Operators

// Basic assignment operator (=)

let assignmentNumber = 8;

// Addition Assignment Operator (+=)

assignmentNumber = assignmentNumber + 2;
console.log('Result of addition assignment operator:' + ' ' + assignmentNumber);

// Shorthand for assignmentNumber = assignmentNumber + 2
assignmentNumber += 2
console.log('Result of addition assignment operator:' + ' ' + assignmentNumber);
// result- 12

// Subtraction/ Multiplication/ Division Operator (-=, *=, /=)
assignmentNumber -= 2
console.log('Result of subtraction operator:' + ' ' + assignmentNumber);
// result- 10

assignmentNumber *= 2
console.log('Result of multiplication operator:' + ' ' + assignmentNumber);
// result- 20

assignmentNumber /= 2
console.log('Result of division operator:' + ' ' + assignmentNumber);
// result- 10

// Arithmetic Operators
let x= 1397;
let y= 7831;

let sum = x + y;
console.log(sum);

let difference = x - y;
console.log(difference);

let product = x * y;
console.log(product);

let quotient = x / y;
console.log(quotient);

let modulo = y % x;
console.log(modulo);

// Multiple Operators and Parentheses

let mdas = 1 + 2 - 3 * 4 / 5;
console.log(mdas);

// PEMDAS

let pemdas = 1 + (2 - 3) * (4 / 5);
console.log(pemdas);

pemdas = (1 + (2 - 3) * (4 / 5));
console.log(pemdas);

// Increment and Decrement

let z = 1;

// pre-increment
let increment = ++z;
console.log(increment);
console.log(z);

// post-increment
let increment1 = z++;
console.log(increment1);
console.log(z);

// pre-decrement
let decrement = --z;
console.log(decrement);
console.log(z);

// post-decrement
let decrement1 = z--;
console.log(z);

// Type Coercion

let numA = '10';
let numB = 12;

let coercion = numA + numB;
console.log(coercion);
console.log(typeof coercion);

let numC = 23;
let numD = 64; 

let nonCoercion = numC + numD;
console.log(nonCoercion);
console.log(typeof nonCoercion);

// true = 1
let numE = true + 1;
console.log(numE);
console.log(typeof numE);

// false = 0
let numF = false + 1;
console.log(numF);

// Comparison Operators

let levi = 'levi';

// Equality operator (==)
	// will return a boolean value
	// not strict with data type

console.log(1 == 1); 
	// result- true
console.log('1' == 1);
	// result- true
console.log(1 == 2);
	// result- false
console.log(1 == true);
	// result- true
console.log('levi' == 'levi');
	// result- true
console.log(levi == 'levi');
	// result- true

// Inequality operator (!=)
	// will return a boolean value
	// not strict with data type

console.log(1 != 1);
	// result- false
console.log(1 != 2);
	// result- true
console.log('levi' != levi);
	// result-false

// Strict Equality Operators (===)

console.log(1 === 1);
	// result- true
console.log(1 === 2);
	// result- false
console.log(1 === '1');
	// result- false
console.log('levi' === levi);
	// result- true

// Strict Inequality Operator (!==)

console.log(1 !== 1);
	// result- false
console.log(1 !== 2);
	// result- true
console.log(0 !== false);
	// result- true
console.log('levi' !== levi);
	// result- false
 
 // Logical Operators
 	// will return a boolean

 let isLegalAge = true;
 let isMarried = false;

 // AND operator (&&- double ampersand)
 // true && true = true
 // true && false = false

 let allRequirementsMet = isLegalAge && isMarried;
 console.log(allRequirementsMet)
 	// result- false

 // OR operator (||- double pipe)
 // true || true = true
 // true || false = true

 let someRequirementsMet = isLegalAge || isMarried;
 console.log(someRequirementsMet)
 	// result- true

// Logical Not Operator (!)
	// returns the opposite value

let someRequirementsNotMet = !isMarried;
console.log(someRequirementsNotMet);
	// result- true

/*BE BACK BY 3:32 PM*/

/*Mini Activity
	What value will it return?
	Try to analyze it without running the code

	let a = true
	let b = false
	let c = true

	let result = !(true && (false || true))
	console.log(result)- false

	Solution:
		1. (false || true)- true
		2. !(true && true)- true
		3. !true- false


*/

// if, else if and else statement

let numG= -1

// if statement
// execute a statement if a specified condition is true
// can stand alone
// syntax: if(){
	// code block
// }

// < - less than
//  >- greater than
// <= - less than or equal
// >= - greater than or equal

if(numG < 0){
	console.log('hello')
};

// else if statement

let numH = 1;

if(numG > 0){
	console.log('hello')
} else if (numH > 0) {
	console.log('World')
};
// result- World

// else statement
// cannot stand alone

if(numG > 0){
	console.log('Hello')
} else if(numH === 0){
	console.log('World')
} else {
	console.log('Again')
}
// result- Again

message = 'no message';
console.log(message);

function determineTyphoonIntensity(windSpeed){

	if(windSpeed < 30){
		return 'Not a typhoon yet'
	}
	else if(windSpeed <= 61){
		return 'Tropical depression detected'
	}
	else if(windSpeed >= 62 && windSpeed <= 88){
		return 'Tropical storm detected'
	}
	else if(windSpeed >= 89 && windSpeed <= 117){
		return 'Severe tropical storm detected'
	}
	else {
		return 'Typhoon detected'
	}
};

message = determineTyphoonIntensity(118);
console.log(message);

if(message == 'Typhoon detected'){
	console.warn(message);
}

// Truthy and Falsy
if(true){
	console.log('truthy')
}

if(0){
	console.log('falsy')
}
/*
	explanation:
	if(1 == 1){
		console.log('truthy')
	}

	if(1 == 2){
		console.log('falsy')
	}

*/
// Ternary Operator (? :)
let ternaryResult = (1 < 18) ? true : false;
console.log(ternaryResult)

// Multiple Statement Execution
let name;

function isOfLegalAge(){
	name= 'Jane';
	return 'You are of the age limit';
}

function isUnderAge(){
	name= 'Janice';
	return 'You are under the age limit';
}

let age = parseInt(prompt('What is your age?'))
console.log(age)

let legalAge = (age > 18) ? isOfLegalAge() : isUnderAge();
console.log(legalAge + ' ' + name)

// Switch Statements

let day = prompt('What day of the week is it today').toLowerCase();

switch(day){
	case 'monday':
	console.log('The color of the day is red');
	break;

	case 'tuesday':
	console.log('The color of the day is blue');
	break;

	case 'wednesday':
	console.log('The color of the day is orange');
	break;

	case 'thursday':
	console.log('The color of the day is yellow');
	break;

	case 'friday':
	console.log('The color of the day is purple');
	break;

	case 'saturday':
	console.log('The color of the day is pink');
	break;

	case 'sunday':
	console.log('The color of the day is white');
	break;

}

// try catch finally statement

function showIntensityAlert(windSpeed){
	try {
		alerat(determineTyphoonIntensity(windSpeed));
	} catch (error){

		console.log(typeof error);

		console.warn(error.message);

	} finally {
		alert('Intensity updated will show new alert')
	}
}
showIntensityAlert(56);
